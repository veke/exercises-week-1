const a = process.argv[2];

if(a === "fi") {
  console.log("Hei Maailma!");
} else if(a === "en" || a === undefined) {
  console.log("Hello World");
} else if(a === "es") {
  console.log("Hola Mundo!");
}

/*ohjelma toimii nyt siis niin, että komentoriville
ohjelman ajo-komennon perään kirjoitetaan joko fi, en tai es.
*/

//a
process.argv.shift(); //poistetaan turhat alkiot varalta
process.argv.shift();

//jahas tajusin vasta tehtävän jälkeen että vaikka luvut on stringinä niin funktio osaa katsoa suurimman.. :D
//sen takia olin siis tehnyt uuden arrayn jossa numerot "integerinä"
//console.log(Math.min(...process.argv));

let arr = [];
for(const luku of process.argv) {
  //console.log(luku);
  arr.push(parseInt(luku));
  //console.log(arr);
}

const number_1 = arr[0];
const number_2 = arr[1];
const number_3 = arr[2];

//console.log(arr.max()); tämä komento ei toimi

//oletan että käyttäjä ei syötä ikinä vain kahta samaa isointa lukua..
if (arr[0] === arr[1] && arr[0] === arr[2]) {
  console.log("kaikki ovat yhtä suuria");
} else if (arr[0] === Math.max(...arr)) {
  console.log(`Suurin luku on number_1 ${number_1}`);
} else if (arr[1] === Math.max(...arr)) {
  console.log(`Suurin luku on number_2 ${number_2}`);
} else if (arr[2] === Math.max(...arr)) {
  console.log(`Suurin luku on number_3 ${number_3}`);
}



//console.log(Math.max(...arr));
//console.log(Math.min(...arr));
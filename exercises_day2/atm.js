const balance = 750;

console.log("using ATM");
console.log("answer always yes or no");

const readline = require("readline");

const RL = readline.createInterface(process.stdin, process.stdout);

RL.question("Check your balance? ", (answer1)=> {
  if(answer1 === "no"){
    console.log("Have a nice day!");
  } else if(answer1 === "yes") {
    RL.question("Is account active and balance > 0? ", (answer2)=> {
      if(answer2 === "yes") {
        console.log(`Your balance is ${balance}`);
      } else if(answer2 === "no") {
        RL.question("Is account not active? ", (answer3)=> {
          if(answer3 === "yes") {
            console.log("Your account is not active");
          } else if(answer3 === "no") {
            RL.question("Is balance = 0? ", (answer4)=> {
              if(answer4 === "yes") {
                console.log("Your account is empty");
              } else if(answer4 === "no") {
                console.log("Your balance is negative");
              }
            });
          }
        });
      }
    });
  }
});